import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_network/base/base_usecase_service.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class BaseUiState<T, RS extends RequestState<T>> extends Equatable {
  abstract final RS? requestState;

  BaseUiState<T, RS> copyWith({RS? requestState});
}

class Wrapped<T> {
  final T value;
  const Wrapped.value(this.value);
}

mixin UiEventParam<PARAM> on BaseUiUIEvent<PARAM> {
  PARAM get param;
}

abstract class BaseUiUIEvent<PARAM> extends Equatable {
  @override
  List<Object> get props => [];
}

class MyBaseBloc<E extends BaseUiUIEvent,
    S extends BaseUiState<dynamic, dynamic>> extends Bloc<E, S> {
  MyBaseBloc(super.initialState);
  CancelToken? cancelToken;

  @override
  Future<void> close() {
    cancelToken?.cancel();
    return super.close();
  }

  void onRequestUsecase<EX extends E, T, PARAM>(
    UseCaseService<T, PARAM> usecase, {
    bool Function(EX event, S state, Emitter<S> emitter)? onCheckRequestCondition,
    S Function(EX event, S state)? initState,
    EventTransformer<EX>? transformer,
    Future<S> Function()? onLoading,
    Future<S> Function(T result)? onSuccessResult,
    Future<S> Function(Exception exception)? onErrorResult,
    Future<S> Function(T? result, Exception? exception)? onFinishResult,
  }) {
    if (<EX>[] is! List<UiEventParam<PARAM>>) {
      throw UnimplementedError(
          "Harus pasang with UiEventParam<$PARAM> (mixin) kalau pake method onRequestUsecase biar tidak kena error. Error di class \"${EX.toString()}\". Contoh pasangnya: class ${EX.toString()} with UiEventParam<$PARAM>");
    }
    onRequest<EX, T>(
      onRequest: (event, state) {
        if (event is UiEventParam<PARAM>) {
          var eventParam = event as UiEventParam<PARAM>;
          return usecase.call(eventParam.param);
        } else {
          throw UnimplementedError(
              "Harus pasang with UiEventParam<$PARAM> (mixin) kalau pake method onRequestUsecase biar tidak kena error. Errornya di \"class ${event.runtimeType}\". Contoh pasangnya: class ${event.runtimeType} with UiEventParam<$PARAM>");
        }
      },
      onCheckRequestCondition: onCheckRequestCondition,
      initState: initState,
      transformer: transformer,
      onLoading: onLoading,
      onSuccessResult: onSuccessResult,
      onErrorResult: onErrorResult,
      onFinishResult: onFinishResult,
    );
  }

  void onRequest<EX extends E, T>({
    required Future<Result<T>> Function(EX event, S state) onRequest,
    bool Function(EX event, S state, Emitter<S> emitter)? onCheckRequestCondition,
    S Function(EX event, S state)? initState,
    EventTransformer<EX>? transformer,
    Future<S> Function()? onLoading,
    Future<S> Function(T result)? onSuccessResult,
    Future<S> Function(Exception exception)? onErrorResult,
    Function(Exception exception)? onCompleteError,
    Function(EX event,T result)? onCompleteSuccess,
    Future<S> Function(T? result, Exception? exception)? onFinishResult,
  }) {
    super.on<EX>((event, emit) async {
      if (onCheckRequestCondition != null &&
          !onCheckRequestCondition.call(event, state, emit)) {
        return;
      }

      if (onLoading == null) {
        if (state.requestState is RequestState<T>) {
          var currentState = (state.requestState as RequestState<T>)
              .copyWith(requestStatus: RequestStatus.loading);
          S iState = initState != null ? initState.call(event, state) : state;
          emit(
            iState.copyWith(requestState: currentState) as S,
          );
        }
      } else {
        S resultLoading = await onLoading.call();
        emit(resultLoading);
      }

      Result<T> result;
      try {
        result = await onRequest(event, state);
      } catch (e) {
        debugPrint("On Catch base_state: $e");
        result = ResultError(error: Exception(e));
      }

      if (result is ResultSuccess) {
        S resultSuccess;
        if (onSuccessResult != null) {
          resultSuccess = await onSuccessResult.call(result.data as T);
        } else {
          var requestState = state.requestState as RequestState<T>;
          var s = state.copyWith(
            requestState: requestState.copyWith(
              data: result.data,
              requestStatus: RequestStatus.success,
            ),
          );
          resultSuccess = s as S;
        }
        emit(resultSuccess);
        onCompleteSuccess?.call(event, result.data as T);
        if (onFinishResult != null) {
          S resultFinish = await onFinishResult.call(result.data, null);
          debugPrint("result: ${resultFinish.requestState}");
          emit(resultFinish);
        }
      } else {
        S resultError;
        if (onErrorResult != null) {
          resultError = await onErrorResult.call(result.error!);
        } else {
          var requestState = state.requestState as RequestState<T>;
          var s = state.copyWith(
            requestState: requestState.copyWith(
              exception: result.error,
              requestStatus: RequestStatus.failure,
            ),
          );
          resultError = s as S;
        }
        emit(resultError);
        onCompleteError?.call(result.error!);
        if (onFinishResult != null) {
          S resultFinish = await onFinishResult.call(null, result.error);
          debugPrint("result: ${resultFinish.requestState}");
          emit(resultFinish);
        }
      }
    }, transformer: transformer);
  }

  // Future<S> toIdle<T>(T? result, Exception? exception) async {
  //   var currentState = (state.requestState as RequestState<T>)
  //       .copyWith(requestStatus: RequestStatus.idle);
  //   return state.copyWith(requestState: currentState) as S;
  // }


  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}

abstract class RequestState<T> extends Equatable {
  abstract final T? data;
  abstract final RequestStatus requestStatus;
  abstract final Exception? exception;

  RequestState copyWith(
      {T? data, RequestStatus? requestStatus, Exception? exception});

  @override
  List<Object?> get props => [data, requestStatus, exception];
}
