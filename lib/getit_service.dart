import 'package:get_it/get_it.dart';

class GetItService {
  static final GetIt _getIt = GetIt.instance;

  static void regIfNotRegisteredFactory<T extends Object>(
      FactoryFunc<T> factoryFunc, {
        String? instanceName,
      }) {
    if (!_getIt.isRegistered<T>(instanceName: instanceName)) {
      _getIt.registerFactory<T>(factoryFunc, instanceName: instanceName);
    }
  }

  static void regIfNotRegisteredSingleton<T extends Object>(
      T instance, {
        String? instanceName,
      }) {
    if (!_getIt.isRegistered<T>(instanceName: instanceName)) {
      _getIt.registerSingleton<T>(instance, instanceName: instanceName);
    }
  }

  static void regIfNotRegisteredLazySingleton<T extends Object>(
      FactoryFunc<T> factoryFunc, {
        String? instanceName,
      }) {
    if (!_getIt.isRegistered<T>(instanceName: instanceName)) {
      _getIt.registerLazySingleton<T>(factoryFunc, instanceName: instanceName);
    }
  }

  static void unRegIfRegistered<T extends Object>({
    String? instanceName,
  }) {
    if (_getIt.isRegistered<T>(instanceName: instanceName)) {
      _getIt.unregister<T>(instanceName: instanceName);
    }
  }

  static T get<T extends Object>({
    String? instanceName,
  }) {
    return _getIt.get<T>(instanceName: instanceName);
  }
}