import 'package:core_bloc/request_status.dart';
import 'package:core_bloc/session/presentation/bloc/session_bloc.dart';
import 'package:core_bloc/session/presentation/bloc/session_bloc_event.dart';
import 'package:core_model/helpers/exception_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:core_model/network/exception/network_exception.dart';
import 'package:core_model/network/exception/network_error_code.dart';

import 'base_state.dart';

class BaseViewPOST<B extends StateStreamable<S>,
    S extends BaseUiState<dynamic, dynamic>> extends StatelessWidget {
  const BaseViewPOST(
      {super.key,
      required this.builder,
      required this.onIdle,
      required this.onSuccess,
      this.onGetRequestStatus,
      this.listener,
      this.listenWhen,
      this.withDialog
      });
  final Widget Function(BuildContext, S) builder;
  final Function() onIdle;
  final Function(S) onSuccess;
  final bool? withDialog;
  final RequestStatus Function(S)? onGetRequestStatus;
  final Function(BuildContext, S)? listener;
  final BlocListenerCondition<S>? listenWhen;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<B, S>(
      listenWhen: listenWhen ?? (previous, current) {
        bool listen = previous.requestState != current.requestState;
        return listen;
      },
      listener: (context, state) {
        var requestState = state.requestState as RequestState;
        void dismissDialog() {
          if(withDialog == null) Navigator.of(context, rootNavigator: true).pop('dialog');
          onIdle();
        }

        final requestStatus = onGetRequestStatus?.call(state) ?? requestState.requestStatus;
        if (requestStatus == RequestStatus.loading) {
          if(withDialog == null) showDialog<String>(
              useRootNavigator: true,
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) {
                return const LoadingProgress();
              }).then((value) => {debugPrint(value)});
        }

        if (requestStatus == RequestStatus.success) {
          if(withDialog == null) dismissDialog();
          onSuccess(state);
        }

        if (requestStatus == RequestStatus.failure) {
          dismissDialog();
          String message = requestState.exception.getMessage(context);
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.red,
            behavior: SnackBarBehavior.floating,
            dismissDirection: DismissDirection.up,
            content: Text(message),
          ));

          if (requestState.exception is NetworkException) {
            var networkEx = requestState.exception as NetworkException;
            if (networkEx.code == NetworkErrorCode.unauthorized) {
              context.read<SessionBloc>().add(OnCheckSession());
            }
          }
        }

        listener?.call(context, state);
      },
      builder: builder,
    );
  }
}

class BaseViewGET<B extends StateStreamable<S>,
    S extends BaseUiState<dynamic, dynamic>> extends StatelessWidget {
  const BaseViewGET({
    super.key,
    required this.successView,
    this.loadingView,
    this.errorView,
    this.onCallRefresh,
    this.listener,
    this.onGetRequestStatus,
    this.listenWhen
  });
  final Widget Function(BuildContext, S) successView;
  final Widget Function(BuildContext, S)? loadingView;
  final Widget Function(BuildContext, S)? errorView;
  final Function()? onCallRefresh;
  final Function(BuildContext, S)? listener;
  final RequestStatus Function(S)? onGetRequestStatus;
  final BlocListenerCondition<S>? listenWhen;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<B, S>(
      listenWhen: listenWhen ?? (previous, current) {
        bool listen = previous.requestState != current.requestState;
        return listen;
      },
      listener: (context, state) {
        var requestState = state.requestState as RequestState;
        final requestStatus = onGetRequestStatus?.call(state) ?? requestState.requestStatus;
        if (requestStatus == RequestStatus.failure) {
          if (requestState.exception is NetworkException) {
            var networkEx = requestState.exception as NetworkException;
            if (networkEx.code == NetworkErrorCode.unauthorized) {
              String message = requestState.exception.getMessage(context);
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                backgroundColor: Colors.red,
                behavior: SnackBarBehavior.floating,
                dismissDirection: DismissDirection.up,
                content: Text(message),
              ));
              context.read<SessionBloc>().add(OnCheckSession());
            }
          }
        }
        listener?.call(context, state);
      },
      builder: (context, state) {
        Widget requestView() {
          var requestState = state.requestState as RequestState;
          final requestStatus = onGetRequestStatus?.call(state) ?? requestState.requestStatus;
          switch (requestStatus) {
            case RequestStatus.loading:
              return loadingView?.call(context, state) ?? LoadingProgress();
            case RequestStatus.success:
              {
                if (onCallRefresh != null) {
                  return RefreshIndicator(
                    onRefresh: () async {
                      onCallRefresh?.call();
                    },
                    child: successView.call(context, state),
                  );
                }
                return successView.call(context, state);
              }
            case RequestStatus.failure:
              {
                if (errorView == null) {
                  String message = requestState.exception.getMessage(context);
                  return ErrorNetworkView(
                    message: message,
                    onCallRefresh: onCallRefresh,
                  );
                } else {
                  return errorView!.call(context, state);
                }
              }
            default:
              return Container();
          }
        }

        return requestView();
      },
    );
  }
}

class ErrorNetworkView extends StatelessWidget {
  const ErrorNetworkView(
      {super.key, required this.message, this.onCallRefresh});
  final String message;
  final Function()? onCallRefresh;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Icon(
              Icons.error_outline_outlined,
              color: Colors.red,
              size: 80,
            ),
            const SizedBox(height: 32),
            Text(
              message,
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Colors.red,
              ),
            ),
            onCallRefresh != null
                ? IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: onCallRefresh,
                    iconSize: 36,
                    alignment: Alignment.center,
                    icon: const Icon(
                      Icons.refresh,
                      color: Colors.black87,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}

class LoadingProgress extends StatelessWidget {
  const LoadingProgress({super.key});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.white, width: 0.2),
            //shape: BoxShape.circle,
            borderRadius: const BorderRadius.all(Radius.circular(16))),
        child: SpinKitFadingCircle(
          color: Theme.of(context).colorScheme.primary,
          size: 50.0,
        ),
      ),
    );
  }
}
