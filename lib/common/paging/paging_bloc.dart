import 'dart:async';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_constants/constants.dart';
import 'package:core_model/network/base/base_paging_param.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/exception/paging_exception.dart';
import 'package:core_network/base/base_usecase_paging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stream_transform/stream_transform.dart';

abstract class RequestPagingState<T> extends RequestState<List<T>> {
  @override
  abstract final List<T> data;

  @override
  RequestPagingState<T> copyWith({
    List<T>? data,
    RequestStatus? requestStatus,
    Exception? exception,
  });

  int get sizeData => data.length;
}

abstract class BasePageState<T, RPS extends RequestPagingState<T>>
    extends BaseUiState<List<T>, RPS> {
  static const bool defaultHasReachedMax = false;

  BasePageState([this.hasReachedMax = defaultHasReachedMax]);

  @override
  abstract final RPS? requestState;

  final bool hasReachedMax;

  @override
  BasePageState<T, RPS> copyWith({RPS? requestState, bool? hasReachedMax});

  @override
  List<Object?> get props => [requestState, hasReachedMax];
}

mixin SearchPagingMixin<T, RPS extends RequestPagingState<T>>
    on BasePageState<T, RPS> {
  String getQuery();
  BasePageState<T, RPS> copyQueryOnly(String? query);
}

class PagingEvent extends BaseUiUIEvent {
  PagingEvent({this.page, this.query = ""});

  final int? page;
  final int initSizePerPage = Constants.initPaging;
  final int nextSizePerPage = Constants.sizePerPage;
  final String query;
}

abstract class PagingBloc<T, E extends PagingEvent,
        S extends BasePageState<T, RequestPagingState<T>>>
    extends MyBaseBloc<E, S> {
  static const Duration defaultDebounce = Duration(milliseconds: 1000);

  PagingBloc(super.initialState);

  // Kalau eventnya extend dan banyak pake ini
  void onEventRequestPaging<Ex extends E, P extends PagingParam>(
      UseCasePaging<T, P> useCasePaging,
      {P Function(PagingParam param, Ex event)? onUpdateParam,
      EventTransformer<Ex>? transformer}) {
    super.on<Ex>(
      (event, emit) async {
        _setQuery(event.query, emit);
        await requestPage(
          emit,
          initSizePerPage: event.initSizePerPage,
          nextSizePerPage: event.nextSizePerPage,
          toPage: event.page,
          qFind: event.query,
          onRequest: (param) {
            P requestParam;
            if (onUpdateParam != null) {
              requestParam = onUpdateParam.call(param, event);
            } else {
              requestParam = param as P;
            }
            return useCasePaging(requestParam);
          },
        );
      },
      transformer: transformer ??
          (state is SearchPagingMixin
              ? queryTransformer<Ex>(defaultDebounce)
              : transformer),
    );
  }

  // kalau jumlah eventnya hanya 1 pake ini aja
  void onRequestPaging<P extends PagingParam>(UseCasePaging<T, P> useCasePaging,
      {P Function(PagingParam param)? onUpdateParam,
      EventTransformer<E>? transformer}) {
    super.on<E>(
      (event, emit) async {
        _setQuery(event.query, emit);
        await requestPage(
          emit,
          initSizePerPage: event.initSizePerPage,
          nextSizePerPage: event.nextSizePerPage,
          toPage: event.page,
          qFind: event.query,
          onRequest: (param) {
            P requestParam;
            if (onUpdateParam != null) {
              requestParam = onUpdateParam.call(param);
            } else {
              requestParam = param as P;
            }
            return useCasePaging(requestParam);
          },
        );
      },
      transformer: transformer ??
          (state is SearchPagingMixin
              ? queryTransformer<E>(defaultDebounce)
              : transformer),
    );
  }

  void _setQuery(String query, Emitter<S> emit) {
    if (state is SearchPagingMixin) {
      final stateMix = state as SearchPagingMixin;
      debugPrint(
          "queryTransformer SearchPagingMixin ${stateMix.getQuery()} == $query");
      if (stateMix.getQuery() != query) {
        emit(stateMix.copyQueryOnly(query) as S);
      }
    }
  }

  Future<void> requestPage(
    Emitter<S> emit, {
    required Future<Result<List<T>>> Function(PagingParam param) onRequest,
    Future<S> Function(List<T> result, RequestStatus status)? onSuccessResult,
    Future<S> Function(
            Exception exception, RequestStatus status, bool isEndOfPage)?
        onErrorResult,
    int initSizePerPage = Constants.sizePerPage,
    int nextSizePerPage = Constants.sizePerPage,
    int? toPage,
    String qFind = "",
  }) async {
    if (state.hasReachedMax && toPage == null) {
      return;
    }

    PagingParam param;

    if (toPage != null) {
      param = PagingParam(toPage, initSizePerPage, q: qFind);
    } else if (state.requestState?.requestStatus == RequestStatus.idle) {
      param = PagingParam(1, initSizePerPage, q: qFind);
    } else {
      final page = (state.requestState?.sizeData ?? 0) ~/ nextSizePerPage;
      param = PagingParam(page + 1, nextSizePerPage, q: qFind);
    }

    final isStartPage = param.page == 1 || toPage != null;
    if (isStartPage) {
      final currentState = state.requestState?.copyWith(
          data: [], requestStatus: RequestStatus.loading, exception: null);
      emit(
        state.copyWith(
          hasReachedMax: false,
          requestState: currentState,
        ) as S,
      );
    }

    final resultList = await onRequest(param);

    final currentState = state.requestState;
    if (resultList is ResultSuccess) {
      List<T> currentData = !isStartPage ? (currentState?.data ?? []) : [];
      List<T> newData = resultList.data ?? [];

      final S resultSuccess;
      if (onSuccessResult == null) {
        resultSuccess = state.copyWith(
            requestState: currentState?.copyWith(
                data: currentData + newData,
                requestStatus: RequestStatus.success)) as S;
      } else {
        resultSuccess =
            await onSuccessResult(currentData + newData, RequestStatus.success);
      }
      emit(resultSuccess.copyWith(hasReachedMax: newData.length < param.size)
          as S);
    } else {
      final hasReachedMax = resultList.error is PagingException;
      final S resultError;
      if (onErrorResult == null) {
        resultError = state.copyWith(
            requestState: currentState?.copyWith(
                exception: resultList.error ?? Exception(),
                requestStatus: hasReachedMax
                    ? RequestStatus.success
                    : RequestStatus.failure)) as S;
      } else {
        resultError = await onErrorResult(
            resultList.error ?? Exception(),
            hasReachedMax ? RequestStatus.success : RequestStatus.failure,
            hasReachedMax);
      }

      emit(resultError.copyWith(hasReachedMax: hasReachedMax) as S);
    }
  }

  EventTransformer<EQ> queryTransformer<EQ extends E>(Duration duration) {
    return (events, mapper) {
      Timer? debounce;
      final debounceTransformer = StreamTransformer<EQ, EQ>.fromHandlers(
        handleData: (data, sink) {
          if (debounce?.isActive ?? false) {
            debounce?.cancel();
          }
          final noDebounce;

          if (state is SearchPagingMixin) {
            final oldQuery = (state as SearchPagingMixin).getQuery();
            debugPrint("queryTransformer $oldQuery == ${data.query}");

            if (data.query.isEmpty && oldQuery.isEmpty && data.page == 1) {
              noDebounce = true;
            } else {
              noDebounce = (data.query.isEmpty && oldQuery == data.query) ||
                  (data.page == null && oldQuery == data.query);
            }
          } else {
            noDebounce = false;
          }
          debugPrint(
              "queryTransformer page: ${data.page}, noDebounce $noDebounce");
          if (noDebounce) {
            sink.add(data);
          } else {
            debounce = Timer(duration, () {
              sink.add(data);
            });
          }
        },
      );
      return droppable<EQ>()
          .call(events.transform(debounceTransformer), mapper);
    };
  }
}
