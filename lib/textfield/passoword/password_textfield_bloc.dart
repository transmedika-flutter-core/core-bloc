import 'package:core_bloc/textfield/passoword/password_textfield_event.dart';
import 'package:core_bloc/textfield/passoword/password_textfield_state.dart';
import 'package:core_bloc/textfield/textfield_validation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc.dart';
import '../textdield_state.dart';
import '../textfield_event.dart';

class PasswordTextFieldBloc extends Bloc<TextFieldEvent,PasswordTextFieldState>{
  PasswordTextFieldBloc({Duration debounceDuration = dropDuration})
      : super(PasswordTextFieldState()) {
    on<OnPasswordTextFieldChange>(_onChange,
        transformer: debounceDuration.inMilliseconds > 0
            ? debounceDroppable(debounceDuration)
            : null);

    on<OnPasswordTextFieldFocus>(_onFocus);
    on<OnPasswordTextFieldShowHide>(_onPasswordShowHide);
  }

  _onChange(OnPasswordTextFieldChange event, Emitter<PasswordTextFieldState> emit) async{
    String? errorString;
    for(TextFieldValidation data in event.validation){
      if(!data.condition){
        errorString = data.errorText;
        break;
      }
    }

    if(errorString!=null){
      emit(state.copyWith(
          text: event.text,
          error: TextFieldError.invalid,
          errorText: errorString
      ));
    }else{
      emit(state.copyWith(
          text: event.text,
          error: TextFieldError.valid
      ));
    }

  }

  _onPasswordShowHide(OnPasswordTextFieldShowHide event, Emitter<PasswordTextFieldState> emit) async{
    if(event.visible){
      emit(state.copyWith(showHidePassword: false, text: event.text));
    }else {
      emit(state.copyWith(showHidePassword: true, text: event.text));
    }
  }

  _onFocus(OnPasswordTextFieldFocus event, Emitter<PasswordTextFieldState> emit) async{
    String? errorString;

    for (TextFieldValidation data in event.validation) {
      if (!data.condition) {
        errorString = data.errorText;
        break;
      }
    }

    if(!event.focus) {
      if (errorString != null) {
        emit(state.copyWith(
            focus: event.focus,
            error: TextFieldError.invalid,
            errorText: errorString
        ));
      } else {
        emit(state.copyWith(
            focus: event.focus,
            error: TextFieldError.valid
        ));
      }
    }else{
      emit(state.copyWith(
          focus: event.focus,
          text: event.text,
          error: errorString == null ? TextFieldError.valid : state.error
      ));
    }
  }

}