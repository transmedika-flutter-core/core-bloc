import 'package:core_bloc/textfield/textfield_validation.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../textfield_event.dart';

class OnPasswordTextFieldChange extends TextFieldEvent {
  OnPasswordTextFieldChange(this.text, this.validation);
  final String? text;
  final List<TextFieldValidation> validation;
}

class OnPasswordTextFieldFocus extends TextFieldEvent {
  OnPasswordTextFieldFocus(this.focus, this.text, this.validation);
  final bool focus;
  final String? text;
  final List<TextFieldValidation> validation;
}

class OnPasswordTextFieldShowHide extends TextFieldEvent {
  OnPasswordTextFieldShowHide({required this.visible, this.text});
  final String? text;
  final bool visible;
}