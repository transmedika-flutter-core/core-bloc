import 'package:flutter/cupertino.dart';

import '../textdield_state.dart';

@immutable
class PasswordTextFieldState extends TextFieldState{
  PasswordTextFieldState({
    this.text,
    this.errorText,
    this.error = TextFieldError.idle,
    this.focus = false,
    this.showHidePassword = false,
  });

  @override
  final String? text;
  @override
  final String? errorText;
  @override
  final TextFieldError? error;
  @override
  final bool focus;
  final bool showHidePassword;

  PasswordTextFieldState copyWith({
    String? text,
    String? errorText,
    TextFieldError? error,
    bool? focus,
    bool? showHidePassword
  }) {
    return PasswordTextFieldState(
        text: text ?? this.text,
        errorText: errorText ?? this.errorText,
        error: error ?? this.error,
        focus: focus ?? this.focus,
        showHidePassword: showHidePassword ?? this.showHidePassword
    );
  }

  @override
  List<Object?> get props => [
    text,
    errorText,
    error,
    focus,
    showHidePassword
  ];
}