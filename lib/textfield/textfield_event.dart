import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class TextFieldEvent extends Equatable{
  @override
  List<Object> get props => [];
}