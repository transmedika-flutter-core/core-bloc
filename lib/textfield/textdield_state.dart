import 'package:equatable/equatable.dart';

enum TextFieldError {idle, invalid, valid}

abstract class TextFieldState extends Equatable{
  abstract final String? text;
  abstract final String? errorText;
  abstract final TextFieldError? error;
  abstract final bool focus;
}