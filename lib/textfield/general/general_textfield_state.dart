import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../textdield_state.dart';


@immutable
class GeneralTextFieldState extends TextFieldState{
  GeneralTextFieldState({
    this.text,
    this.errorText,
    this.error = TextFieldError.idle,
    this.focus = false,
  });

  @override
  final String? text;
  @override
  final String? errorText;
  @override
  final TextFieldError? error;
  @override
  final bool focus;

  GeneralTextFieldState copyWith({
    String? text,
    String? errorText,
    TextFieldError? error,
    bool? focus
  }) {
    return GeneralTextFieldState(
        text: text ?? this.text,
        errorText: errorText ?? this.errorText,
        error: error ?? this.error,
        focus: focus ?? this.focus
    );
  }

  @override
  List<Object?> get props => [
    text,
    errorText,
    error,
    focus
  ];
}