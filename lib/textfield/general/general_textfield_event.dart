import 'package:core_bloc/textfield/textfield_validation.dart';

import '../textfield_event.dart';

class OnGeneralTextFieldChange extends TextFieldEvent {
  OnGeneralTextFieldChange(this.text, this.validation);
  final String? text;
  final List<TextFieldValidation>? validation;
}

class OnGeneralTextFieldSetText extends TextFieldEvent {
  OnGeneralTextFieldSetText(this.text);
  final String? text;
}

class OnGeneralTextFieldFocus extends TextFieldEvent {
  OnGeneralTextFieldFocus(this.focus, this.text, this.validation);
  final bool focus;
  final String? text;
  final List<TextFieldValidation>? validation;
}