import 'package:core_bloc/textfield/general/general_textfield_event.dart';
import 'package:core_bloc/textfield/general/general_textfield_state.dart';
import 'package:core_bloc/textfield/textfield_validation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc.dart';
import '../textdield_state.dart';
import '../textfield_event.dart';

class GeneralTextFieldBloc extends Bloc<TextFieldEvent, GeneralTextFieldState> {
  GeneralTextFieldBloc({Duration debounceDuration = dropDuration})
      : super(GeneralTextFieldState()) {
    on<OnGeneralTextFieldChange>(_onChange,
        transformer: debounceDuration.inMilliseconds > 0
            ? debounceDroppable(debounceDuration)
            : null);
    on<OnGeneralTextFieldFocus>(_onFocus);
    on<OnGeneralTextFieldSetText>(_onGeneralTextFieldSetText);
  }

  _onGeneralTextFieldSetText(OnGeneralTextFieldSetText event, Emitter<GeneralTextFieldState> emit) async{
    emit(state.copyWith(text: event.text));
  }

  _onChange(OnGeneralTextFieldChange event,
      Emitter<GeneralTextFieldState> emit) async {
    String? errorString;

    if (event.validation != null) {
      for (TextFieldValidation data in event.validation!) {
        if (!data.condition) {
          errorString = data.errorText;
          break;
        }
      }
    }

    if (errorString != null) {
      emit(state.copyWith(
          text: event.text,
          error: TextFieldError.invalid,
          errorText: errorString));
    } else {
      emit(state.copyWith(text: event.text, error: TextFieldError.valid));
    }
  }

  _onFocus(OnGeneralTextFieldFocus event,
      Emitter<GeneralTextFieldState> emit) async {
    String? errorString;

    if (event.validation != null) {
      for (TextFieldValidation data in event.validation!) {
        if (!data.condition) {
          errorString = data.errorText;
          break;
        }
      }
    }

    if (!event.focus) {
      if (errorString != null) {
        emit(state.copyWith(
            text: event.text,
            focus: event.focus,
            error: TextFieldError.invalid,
            errorText: errorString));
      } else {
        emit(state.copyWith(
            text: event.text, focus: event.focus, error: TextFieldError.valid));
      }
    } else {
      emit(state.copyWith(
          focus: event.focus,
          text: event.text,
          error: errorString == null ? TextFieldError.valid : state.error));
    }
  }
}
