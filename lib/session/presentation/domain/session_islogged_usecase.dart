import 'package:core_data/main/main_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_network/base/base_usecase_service.dart';

class SessionIsLoggedUsecase implements UseCaseService<bool, void> {
  final MainRepositoryService mainRepositoryService;

  SessionIsLoggedUsecase({required this.mainRepositoryService});

  @override
  Future<Result<bool>> call([void param]) async {
    var result = await mainRepositoryService.getSession();
    if (result != null) {
      return ResultSuccess(data: true);
    }
    return ResultError();
  }
}
