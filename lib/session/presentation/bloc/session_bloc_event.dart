import 'package:core_bloc/base_state.dart';

abstract class SessionUIEvent extends BaseUiUIEvent {}

class OnCheckSession extends SessionUIEvent {}
class TestLogoutkSession extends SessionUIEvent {}