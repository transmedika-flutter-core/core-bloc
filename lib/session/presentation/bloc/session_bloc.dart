import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_bloc/session/presentation/domain/session_islogged_usecase.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:core_bloc/session/presentation/bloc/session_bloc_event.dart';
import 'package:core_bloc/session/presentation/bloc/session_bloc_state.dart';

class SessionBloc extends MyBaseBloc<SessionUIEvent, SessionUIState> {
  SessionBloc({required SessionIsLoggedUsecase sessionIsLoggedUsecase, bool statusLoggedInit = false})
      : _sessionIsLoggedUsecase = sessionIsLoggedUsecase,
        super(
          SessionUIState(
            requestState: SessionState(data: statusLoggedInit),
          ),
        ) {
    on<OnCheckSession>(_onCheckSession);
    on<TestLogoutkSession>(_onTestLogoutSession);
  }

  final SessionIsLoggedUsecase _sessionIsLoggedUsecase;

  _onCheckSession(OnCheckSession event, Emitter<SessionUIState> emitter) async {
    final result = await _sessionIsLoggedUsecase();
    SessionState sessionState;
    if (result is ResultSuccess) {
      sessionState = state.requestState!
          .copyWith(data: true, requestStatus: RequestStatus.success);
    } else {
      sessionState = state.requestState!
          .copyWith(data: false, requestStatus: RequestStatus.failure);
    }
    emitter(state.copyWith(requestState: sessionState));
  }

  _onTestLogoutSession(
    TestLogoutkSession event,
    Emitter<SessionUIState> emitter,
  ) {
    var sessionState = state.requestState!
        .copyWith(data: false, requestStatus: RequestStatus.failure);
    emitter(state.copyWith(requestState: sessionState));
  }
}
