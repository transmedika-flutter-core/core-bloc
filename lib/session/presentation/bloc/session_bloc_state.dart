import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';

class SessionState extends RequestState<bool> {
  @override
  final bool data;
  @override
  final RequestStatus requestStatus;
  @override
  final Exception? exception;

  SessionState(
      {this.data = false, this.requestStatus = RequestStatus.idle, this.exception});

  @override
  SessionState copyWith(
      {dynamic data, RequestStatus? requestStatus, Exception? exception}) {
    return SessionState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception);
  }
}

class SessionUIState extends BaseUiState<bool, SessionState> {
  SessionUIState({this.requestState});

  @override
  final SessionState? requestState;

  @override
  SessionUIState copyWith({
    SessionState? requestState,
  }) {
    return SessionUIState(requestState: requestState ?? this.requestState);
  }

  @override
  List<Object?> get props => [requestState];
}
